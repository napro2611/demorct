import { createActions } from 'reduxsauce'

/* ------------- Types and Action Creators ------------- */
const { Types, Creators } = createActions({
  getData: ['data'],
  restApiRequest: ['data'],
  getDataSuccess: ['data'],
  failure: ['error']
})

export const DemoAction = Creators
export const DemoActionCode = Types