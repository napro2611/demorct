import React, { Component } from 'react'
import { NavigationActions, StackActions } from 'react-navigation'
import { Image, ScrollView, Text, View } from 'react-native'
import { Images } from '../../Themes/index'
// Styles
import styles from './Detail.Styles'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { DemoAction } from './Detail.Action';
import { connect } from 'react-redux'

class DetailScreen extends Component {

  constructor(props){
    super(props)
    this.state = {
      //nameA : { a: 'dgdg', b: 'ddbd', c : {a: 'dddggd'}}
    }
  }

  render () {
    const {params}= this.props.navigation.state
    //const {nameA, nameA: {a, b, c : {a}={}}= {}} = this.state
    let name1 = params ? params.name : '123'
    
    const {data, isFetching} = this.props.detail
    console.log('TEST NAME', name1)
    return (
      <View style={styles.mainContainer}>
        <ScrollView style={styles.container}>
          <View style={styles.centered}>
            <Image source={Images.launch} style={styles.logo}/>
          </View>

          <TouchableOpacity onPress={this.callApi} style={styles.section}>
            <Image source={Images.ready}/>
            <Text style={[styles.sectionText, {color: '#000'}]}>
              {'Detail' + name1}
            </Text>
          </TouchableOpacity>
          {
            isFetching ? <Text style={{color: '#000'}}>Loding...</Text> : null
          }
        </ScrollView>
      </View>
    )
  }

}

callApi=()=>{
   let body ={
     username: 'thanhit93'
   }
   this.props.getGithubUser(DemoAction.restApiRequest(body))
}

function mapStateToProps(state) {
  return {
    detail: state.detail
  }
}

function mapDispatchToProps(dispatch) {
  return {
    getFakeData: request => dispatch(request),
    getGithubUser: request => dispatch(request)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailScreen)

