import apisauce from 'apisauce'

const create = (baseURL = 'https://api.github.com/') => {
  const api = apisauce.create({
    baseURL,
    headers: {
      'Cache-Control': 'no-cache'
    },
    timeout: 10000
  })
  const getUser = body => api.get('search/users', { q: body.username })

  //Fake data
  const getFakeDataApi = () => {
    return 'Fake data'
  }

  return {
    getFakeDataApi,
    getUser
  }
}

export default {
  create
}