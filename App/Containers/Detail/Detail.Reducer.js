
import { call, put } from 'redux-saga/effects'
import { createReducer } from 'reduxsauce'
import Immutable from 'seamless-immutable'

// Import Action and Api services
import { DemoAction, DemoActionCode } from './Detail.Action'
import Api from './Detail.Api'
// Export ActionCode and Services
export default DemoActionCode
export const DemoServices = Api.create()

/* ------------- Business logic ------------- */
export const DemoLogicFunc = {
  getData,
  getRestData
}

function* getData(api, action) {
  const response = yield call(api.getFakeDataApi)
  yield put(DemoAction.getDataSuccess(response))
}

function* getRestData(api, action) {
  const { data } = action
  const response = yield call(api.getUser, data)
  console.log(response)
  if (response.ok && response.status === 200) {
    yield put(DemoAction.getDataSuccess(response.data))
  } else {
    yield put(DemoAction.failure(response.data))
  }
}

/* ------------- Initial State ------------- */
const INITIAL_STATE = Immutable({
  data: null,
  isFetching: false,
  error: false
})

/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
  [DemoActionCode.GET_DATA]: (state, action) => {
    return {
      ...state,
      data: null,
      isFetching: true,
      error: false
    }
  },
  [DemoActionCode.REST_API_REQUEST]: (state, action) => {
    return {
      ...state,
      data: null,
      isFetching: true,
      error: false
    }
  },
  [DemoActionCode.GET_DATA_SUCCESS]: (state, action) => {
    return {
      ...state,
      data: action.data,
      isFetching: false,
      error: false
    }
  },
  [DemoActionCode.FAILURE]: (state, action) => {
    return {
      ...state,
      data: action.error,
      isFetching: false,
      error: true
    }
  }
})