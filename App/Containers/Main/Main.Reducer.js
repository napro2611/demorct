
import { call, put } from 'redux-saga/effects'
import { createReducer } from 'reduxsauce'
import Immutable from 'seamless-immutable'

// Import Action and Api services
import { DemoAction, DemoActionCode } from './Main.Action'
import Api from './Main.Api'
// Export ActionCode and Services
export default DemoActionCode
export const DemoServices = Api.create()

/* ------------- Business logic ------------- */
export const DemoGetUser = {
    getUserGit,
    refreshUserGit,
}

function* getUserGit(api, action) {
    console.log('call')
    const response = yield call(api.getUserGit, action.data)
    if (response.ok && response.status === 200) {
        yield put(DemoAction.getUserGitSuccess(response.data, action.data))
    } else {
        yield put(DemoAction.getUserGitFail(response.data))
    }
}

function* refreshUserGit(api, action) {
    const response = yield call(api.getUserGit, action.data)
    if (response.ok && response.status === 200) {
        yield put(DemoAction.refreshUserGitSuccess(response.data))
    } else {
        yield put(DemoAction.refreshUserGitFail(response.data))
    }
}

/* ------------- Initial State ------------- */
const INITIAL_STATE = {
    data: [],
    page:1,
    isFetching: false,
    isRefreshing: false,
    error: false
};

/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
    [DemoActionCode.GET_USER_GIT]: (state, action) => {
        return {
            ...state,
            isFetching: true,
            error: false
        }
    },
    [DemoActionCode.GET_USER_GIT_SUCCESS]: (state, action) => {
        return {
            ...state,
            isFetching: false,
            page: action.params.page,
            error: false,
            data: state.data.concat(action.data.items)
        }
    },
    [DemoActionCode.GET_USER_GIT_FAIL]: (state, action) => {
        return {
            ...state,
            data: [],
            isFetching: false,
            error: false
        }
    },
    [DemoActionCode.REFRESH_USER_GIT]: (state, action) => {
        return {
            ...state,
            data: [],
            isRefreshing: true,
            error: false
        }
    },
    [DemoActionCode.REFRESH_USER_GIT_SUCCESS]: (state, action) => {
        return {
            ...state,
            data: action.data.items,
            page: 1,
            isRefreshing: false,
            error: false
        }
    }
})