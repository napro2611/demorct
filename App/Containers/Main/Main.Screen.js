import React, { Component } from 'react'
import { View, Image, ActivityIndicator, FlatList } from 'react-native'
import { Body, Container, Content, Header, Title, Button, Icon, Text } from 'native-base'

import MainStyles from './Main.Styles'
import Config from 'react-native-config'

import { connect } from 'react-redux'
import { DemoAction } from './Main.Action';

class MainScreen extends Component {

  componentDidMount = () => {
    // console.log(DemoAction.getUserGit({
    //   name: 'John',
    //   page: 1
    // }));
    this.props.getGithubUser(DemoAction.getUserGit({
      name: 'John',
      page: 1
    }))
  }

  handleLoadMore = () => {
    console.log('loadmore')
    this.props.getGithubUser(DemoAction.getUserGit({
      name: 'John',
      page: this.props.page + 1
    }))
  }


  _renderItem = (item) => {
    return (
      <View style={MainStyles.itemContainer}>
        <Image style={MainStyles.avatar}
          source={{ uri: item.avatar_url }}
          style={MainStyles.avatar}
          resizeMode="cover"
        />
        <View style={MainStyles.information}>
          <Text style={MainStyles.name}>{item.login}</Text>
          <Text style={MainStyles.info}>{item.type}</Text>
          <Text style={MainStyles.info}>{item.score}</Text>
        </View>
      </View>
    )
  }

  _onRefresh = () => {
    this.props.refreshGitUser(DemoAction.refreshUserGit({
      name: 'John',
      page: 1
    }))
  }

  renderFooter = () => {
    if (this.props.isFetching) {
      return (
        <ActivityIndicator size="large" style={{ marginVertical: 30 }} color="red" />
      )
    }
    return (
      <Content>
        <Button iconLeft primary onPress={() => this.handleLoadMore()}>
          <Icon name='beer' />
          <Text>Load more</Text>
        </Button>
      </Content>
    )
  }

  renderHeader = () => {
    if (this.props.isRefreshing) {
      return (
        <ActivityIndicator size="large" style={{ marginVertical: 30 }} color="red" />
      )
    }
    return (
      <Content>
        <Button iconLeft primary onPress={() => this._onRefresh()}>
          <Icon name='beer' />
          <Text>Refresh</Text>
        </Button>
      </Content>
    )
  }

  render() {
    console.log(this.props)
    return (
      <Container>
        <Header>
          <Body>
            <Title>{Config.ROOT_URL}</Title>
          </Body>
        </Header>
        <Content>

          <View style={MainStyles.container}>
            <FlatList
              data={this.props.listUser}
              renderItem={({ item }) => this._renderItem(item)}
              ListFooterComponent={() => this.renderFooter()}
              ListHeaderComponent={() => this.renderHeader()}
            />
          </View>

        </Content>
      </Container>
    )
  }

  onGo = () => {
    this.props.navigation.navigate('Detail1Screen')
  }
}

const mapStateToProps = state => {
  return {
    listUser: state.main.data,
    isFetching: state.main.isFetching,
    page: state.main.page,
    isRefreshing: state.main.isRefreshing,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    getGithubUser: request => dispatch(request),
    refreshGitUser: request => dispatch(request)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainScreen);


const data = [
  {
    "login": "dangtrinhnt",
    "id": 2570623,
    "node_id": "MDQ6VXNlcjI1NzA2MjM=",
    "avatar_url": "https://avatars2.githubusercontent.com/u/2570623?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/dangtrinhnt",
    "html_url": "https://github.com/dangtrinhnt",
    "followers_url": "https://api.github.com/users/dangtrinhnt/followers",
    "following_url": "https://api.github.com/users/dangtrinhnt/following{/other_user}",
    "gists_url": "https://api.github.com/users/dangtrinhnt/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/dangtrinhnt/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/dangtrinhnt/subscriptions",
    "organizations_url": "https://api.github.com/users/dangtrinhnt/orgs",
    "repos_url": "https://api.github.com/users/dangtrinhnt/repos",
    "events_url": "https://api.github.com/users/dangtrinhnt/events{/privacy}",
    "received_events_url": "https://api.github.com/users/dangtrinhnt/received_events",
    "type": "User",
    "site_admin": false,
    "score": 57.794495
  },
  {
    "login": "trinhdung0911",
    "id": 18625977,
    "node_id": "MDQ6VXNlcjE4NjI1OTc3",
    "avatar_url": "https://avatars0.githubusercontent.com/u/18625977?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/trinhdung0911",
    "html_url": "https://github.com/trinhdung0911",
    "followers_url": "https://api.github.com/users/trinhdung0911/followers",
    "following_url": "https://api.github.com/users/trinhdung0911/following{/other_user}",
    "gists_url": "https://api.github.com/users/trinhdung0911/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/trinhdung0911/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/trinhdung0911/subscriptions",
    "organizations_url": "https://api.github.com/users/trinhdung0911/orgs",
    "repos_url": "https://api.github.com/users/trinhdung0911/repos",
    "events_url": "https://api.github.com/users/trinhdung0911/events{/privacy}",
    "received_events_url": "https://api.github.com/users/trinhdung0911/received_events",
    "type": "User",
    "site_admin": false,
    "score": 57.111095
  },
  {
    "login": "tony19",
    "id": 26580,
    "node_id": "MDQ6VXNlcjI2NTgw",
    "avatar_url": "https://avatars1.githubusercontent.com/u/26580?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/tony19",
    "html_url": "https://github.com/tony19",
    "followers_url": "https://api.github.com/users/tony19/followers",
    "following_url": "https://api.github.com/users/tony19/following{/other_user}",
    "gists_url": "https://api.github.com/users/tony19/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/tony19/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/tony19/subscriptions",
    "organizations_url": "https://api.github.com/users/tony19/orgs",
    "repos_url": "https://api.github.com/users/tony19/repos",
    "events_url": "https://api.github.com/users/tony19/events{/privacy}",
    "received_events_url": "https://api.github.com/users/tony19/received_events",
    "type": "User",
    "site_admin": false,
    "score": 56.1547
  },
  {
    "login": "trinhngockhang",
    "id": 26276956,
    "node_id": "MDQ6VXNlcjI2Mjc2OTU2",
    "avatar_url": "https://avatars3.githubusercontent.com/u/26276956?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/trinhngockhang",
    "html_url": "https://github.com/trinhngockhang",
    "followers_url": "https://api.github.com/users/trinhngockhang/followers",
    "following_url": "https://api.github.com/users/trinhngockhang/following{/other_user}",
    "gists_url": "https://api.github.com/users/trinhngockhang/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/trinhngockhang/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/trinhngockhang/subscriptions",
    "organizations_url": "https://api.github.com/users/trinhngockhang/orgs",
    "repos_url": "https://api.github.com/users/trinhngockhang/repos",
    "events_url": "https://api.github.com/users/trinhngockhang/events{/privacy}",
    "received_events_url": "https://api.github.com/users/trinhngockhang/received_events",
    "type": "User",
    "site_admin": false,
    "score": 55.816402
  },
  {
    "login": "TrinhDinhPhuc",
    "id": 14198992,
    "node_id": "MDQ6VXNlcjE0MTk4OTky",
    "avatar_url": "https://avatars2.githubusercontent.com/u/14198992?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/TrinhDinhPhuc",
    "html_url": "https://github.com/TrinhDinhPhuc",
    "followers_url": "https://api.github.com/users/TrinhDinhPhuc/followers",
    "following_url": "https://api.github.com/users/TrinhDinhPhuc/following{/other_user}",
    "gists_url": "https://api.github.com/users/TrinhDinhPhuc/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/TrinhDinhPhuc/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/TrinhDinhPhuc/subscriptions",
    "organizations_url": "https://api.github.com/users/TrinhDinhPhuc/orgs",
    "repos_url": "https://api.github.com/users/TrinhDinhPhuc/repos",
    "events_url": "https://api.github.com/users/TrinhDinhPhuc/events{/privacy}",
    "received_events_url": "https://api.github.com/users/TrinhDinhPhuc/received_events",
    "type": "User",
    "site_admin": false,
    "score": 53.6048
  },
  {
    "login": "trinhnv89",
    "id": 1867102,
    "node_id": "MDQ6VXNlcjE4NjcxMDI=",
    "avatar_url": "https://avatars0.githubusercontent.com/u/1867102?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/trinhnv89",
    "html_url": "https://github.com/trinhnv89",
    "followers_url": "https://api.github.com/users/trinhnv89/followers",
    "following_url": "https://api.github.com/users/trinhnv89/following{/other_user}",
    "gists_url": "https://api.github.com/users/trinhnv89/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/trinhnv89/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/trinhnv89/subscriptions",
    "organizations_url": "https://api.github.com/users/trinhnv89/orgs",
    "repos_url": "https://api.github.com/users/trinhnv89/repos",
    "events_url": "https://api.github.com/users/trinhnv89/events{/privacy}",
    "received_events_url": "https://api.github.com/users/trinhnv89/received_events",
    "type": "User",
    "site_admin": false,
    "score": 53.588
  },
  {
    "login": "TechMaster",
    "id": 1491686,
    "node_id": "MDQ6VXNlcjE0OTE2ODY=",
    "avatar_url": "https://avatars3.githubusercontent.com/u/1491686?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/TechMaster",
    "html_url": "https://github.com/TechMaster",
    "followers_url": "https://api.github.com/users/TechMaster/followers",
    "following_url": "https://api.github.com/users/TechMaster/following{/other_user}",
    "gists_url": "https://api.github.com/users/TechMaster/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/TechMaster/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/TechMaster/subscriptions",
    "organizations_url": "https://api.github.com/users/TechMaster/orgs",
    "repos_url": "https://api.github.com/users/TechMaster/repos",
    "events_url": "https://api.github.com/users/TechMaster/events{/privacy}",
    "received_events_url": "https://api.github.com/users/TechMaster/received_events",
    "type": "User",
    "site_admin": false,
    "score": 53.344368
  },
  {
    "login": "trinhd",
    "id": 15623817,
    "node_id": "MDQ6VXNlcjE1NjIzODE3",
    "avatar_url": "https://avatars1.githubusercontent.com/u/15623817?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/trinhd",
    "html_url": "https://github.com/trinhd",
    "followers_url": "https://api.github.com/users/trinhd/followers",
    "following_url": "https://api.github.com/users/trinhd/following{/other_user}",
    "gists_url": "https://api.github.com/users/trinhd/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/trinhd/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/trinhd/subscriptions",
    "organizations_url": "https://api.github.com/users/trinhd/orgs",
    "repos_url": "https://api.github.com/users/trinhd/repos",
    "events_url": "https://api.github.com/users/trinhd/events{/privacy}",
    "received_events_url": "https://api.github.com/users/trinhd/received_events",
    "type": "User",
    "site_admin": false,
    "score": 53.002956
  },
  {
    "login": "chtrinh",
    "id": 70832,
    "node_id": "MDQ6VXNlcjcwODMy",
    "avatar_url": "https://avatars2.githubusercontent.com/u/70832?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/chtrinh",
    "html_url": "https://github.com/chtrinh",
    "followers_url": "https://api.github.com/users/chtrinh/followers",
    "following_url": "https://api.github.com/users/chtrinh/following{/other_user}",
    "gists_url": "https://api.github.com/users/chtrinh/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/chtrinh/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/chtrinh/subscriptions",
    "organizations_url": "https://api.github.com/users/chtrinh/orgs",
    "repos_url": "https://api.github.com/users/chtrinh/repos",
    "events_url": "https://api.github.com/users/chtrinh/events{/privacy}",
    "received_events_url": "https://api.github.com/users/chtrinh/received_events",
    "type": "User",
    "site_admin": false,
    "score": 52.8406
  },
  {
    "login": "trinhudo",
    "id": 43434724,
    "node_id": "MDQ6VXNlcjQzNDM0NzI0",
    "avatar_url": "https://avatars2.githubusercontent.com/u/43434724?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/trinhudo",
    "html_url": "https://github.com/trinhudo",
    "followers_url": "https://api.github.com/users/trinhudo/followers",
    "following_url": "https://api.github.com/users/trinhudo/following{/other_user}",
    "gists_url": "https://api.github.com/users/trinhudo/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/trinhudo/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/trinhudo/subscriptions",
    "organizations_url": "https://api.github.com/users/trinhudo/orgs",
    "repos_url": "https://api.github.com/users/trinhudo/repos",
    "events_url": "https://api.github.com/users/trinhudo/events{/privacy}",
    "received_events_url": "https://api.github.com/users/trinhudo/received_events",
    "type": "User",
    "site_admin": false,
    "score": 50.374798
  }
]