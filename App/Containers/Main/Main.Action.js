import { createActions } from 'reduxsauce'

/* ------------- Types and Action Creators ------------- */
const { Types, Creators } = createActions({
  getUserGit: ['data'],
  refreshUserGit: ['data'],
  refreshUserGitSuccess: ['data'],
  refreshUserGitFail: ['data'],
  getUserGitSuccess: ['data','params'],
  getUserGitFail: ['data'],
})

export const DemoAction = Creators
export const DemoActionCode = Types