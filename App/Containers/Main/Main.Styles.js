import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    backgroundColor: 'transparent'
  },
  center: {
    alignSelf: 'center'
  },
  itemContainer: {
    paddingHorizontal: 24,
    marginVertical: 8,
    flexDirection: 'row'
  },
  avatar: {
    width: 56,
    height: 56,
    borderRadius: 28,
  },
  information: {
    marginLeft: 16,
  },
  name: {
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    fontSize: 16,
  },
  info: {
    fontFamily: 'Roboto',
    fontSize: 14,
    marginVertical: 4,
  }
})
