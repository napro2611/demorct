import apisauce from 'apisauce'

const create = (baseURL = 'https://api.github.com/') => {
  const api = apisauce.create({
    baseURL,
    headers: {
      'Cache-Control': 'no-cache'
    },
    timeout: 10000
  })
  const getUserGit = body => {
      const linkUrl = `search/users?q=${body.name}&per_page=10&page=${body.page}`
     return api.get(linkUrl)
  }

  return {
    getUserGit
  }
}

export default {
  create
}