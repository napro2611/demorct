import { all, takeLatest } from 'redux-saga/effects'
import { StartupTypes } from '../Redux/StartupRedux'
import { startup } from './StartupSagas'
import { DemoActionCode } from '../Containers/Main/Main.Action';
import { DemoGetUser, DemoServices } from '../Containers/Main/Main.Reducer';

/* ------------ REDUX ------------ */
//@nhancv 2019-03-11
//TODO: Add REDUX here: Action, Function, Service

/* ------------- Connect Types To Sagas ------------- */

export default function * root () {
  yield all([
    // some sagas only receive an action
    takeLatest(StartupTypes.STARTUP, startup),

    takeLatest(DemoActionCode.GET_USER_GIT, DemoGetUser.getUserGit, DemoServices),
    takeLatest(DemoActionCode.REFRESH_USER_GIT, DemoGetUser.refreshUserGit, DemoServices)
    //@nhancv 2019-03-11
    //TODO: redux flow configuration

  ])
}
